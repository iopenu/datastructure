package net.openu.datastructure.arraylist.implementation;



public class ArrayList {

	private int size = 0;
	private Object[] elementData;
	public ArrayList(){
		elementData = new Object[100];
	}

	//가장 끝에 배열을 추가하는 addLast;
	public boolean addLast(Object element){
		elementData[size] = element;
		size++;
		return true;
	}

	//배열중간 index에 element추가
	public boolean add(int index, Object element) {
		for(int i = size -1;i>=index;i--){
			elementData[i+1] = elementData[i];
		}
		elementData[index] = element;
		size++;
		return true;
	}

	//배열에 맨앞에 element추가
	public boolean addFirst(Object element){
		add(0,element);
		return true;
	}

	//배열에 index element 삭제
	public Object remove(int index){
		Object removed  = elementData[index];
		for(int i=index+1;i<=size-1;i++){
			System.out.println("i->"+i);
			elementData[i-1] = elementData[i];
		}
		elementData[size-1] = null;
		size--;

		return removed;
	}

	//배열에 맨처음 엘리먼트 삭제
	public Object removeFirst(){
		return remove(0);
	}

	//배열에 맨마지막 엘리먼트 삭제
	public Object removeLast(){
		return remove(size-1);
	}

	//특정값을 가진 element찾기 (순서로 처음것만)
	public Object indexOf(Object o){
		Object retVal = -1;

		for(int i=0;i<=size;i++){
			if(o.equals(elementData[i])){
				return i;
			}
		}

		return retVal;
	}

	public Object get(int index){
		return elementData[index];
	}


	public int size(){
		return size;
	}

	public String toString(){
		String str = "[";
		for(int i=0;i<size;i++){
			str += elementData[i];
			if(i< size-1){
				str += ",";
			}
		}
		return str+"]";
	}


	public ListIterator listIterator() {
		return new ListIterator();
	}

	public class ListIterator{
		private int nextIndex = 0;
		public  Object next(){
//			Object returnData =  elementData[nextIndex];
//			nextIndex++;
//			return returnData;
			return elementData[nextIndex++];
		}

		public Object previous(){
			return elementData[--nextIndex];
		}

		public boolean hasNext() {
			return nextIndex <size();
		}

		public boolean hasPrevious() {
			return nextIndex >0;
		}

		public void add(Object element){
			ArrayList.this.add(nextIndex++,element);
		}

		public void remove(Object element){
			ArrayList.this.remove(nextIndex-1);
			nextIndex--;
		}


	}
}
