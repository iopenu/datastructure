package net.openu.datastructure.arraylist.implementation;

public class ArrayListMain {
	public static void main(String[] args) {

		System.out.println("hello");

		ArrayList numbers = new ArrayList();
		numbers.addLast(10);
		numbers.addLast(20);
		numbers.addLast(30);
		numbers.addLast(40);

		//중간에 추가
		numbers.add(1,15);

		System.out.println(numbers.toString());

		numbers.remove(2);
		System.out.println(numbers.toString());
	//	System.out.println(numbers.indexOf(50));

		for(int i=0;i<numbers.size();i++){
			System.out.println(numbers.get(i));
		}

		System.out.println("=========================");

		ArrayList.ListIterator li =  numbers.listIterator();
		while (li.hasNext()){
			System.out.println(li.next());
		}
		System.out.println("=========================");
		while (li.hasPrevious()){
			System.out.println(li.previous());
		}


	}
}
